Rails.application.routes.draw do
  get 'colleges/show'
  devise_for :students,controllers: { registrations: 'students/registrations' }
  get 'home/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  resources :students, only: [:show,:edit,:update]
  resources :colleges, only: [:index,:new, :create,:show]
end
