# README



Things you may want to cover:

Note : Please go to the application folder and execute all the commands mentioned below

* Ruby version

	version : 2.4.0

	https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-16-04

* Rails Version
	
	gem install rails -v 5.2.1

* System dependencies

	- run "bundle install" to install the dependencies of rails application

* Install Redis 
	
	-Ubuntu : 
		installation : sudo apt-get install redis-server
		To run : sudo systemctl enable redis-server.service

	-mac :
		installation : brew install redis
		To run : redis-server /usr/local/etc/redis.conf

* Database initialization

	- Mysql
	- Please set your mysql credentials in config/database.yml

* Database creation

	- Firstly create database mentioned in the config/database.yml according to the environment

* Database Migration
	
	- run "rails db:migrate" for migrting the database schema.

* To run the Application

	run "rails server"
