class College < ApplicationRecord
	has_many :students, dependent: :destroy
	has_one_attached :logo
end
