class CollegesController < ApplicationController
	before_action :authenticate_student!
	def new
	end

	def index
		@colleges = College.all
	end

	def show
  		@college = College.find(params[:id])
        Rails.cache.write(params[:id],Rails.cache.fetch(params[:id]){0}+1)
        Rails.cache.write("current"+params[:id]+"_"+current_student[:email],current_student[:email],expires_in: 10.minute)
        @user_arr = []
        $redis.keys("current"+params[:id]+"*_*").each do |elem|
        	@user_arr.push(Rails.cache.read(elem))
        end
 	end

	def create
	  College.create(post_params)
	  redirect_to colleges_path
	end
	
	private
	def post_params
	  params.require(:college).permit(:name, :address, :current_views, :total_views,:logo)
	end

end
