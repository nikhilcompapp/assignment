class HomeController < ApplicationController
  before_action :authenticate_student!

  def index
  	redirect_to current_student
  end
end
