class StudentsController < ApplicationController
	def show
	end

	def edit
		@student = Student.find(params[:id])
		@colleges = College.all
	end

	def update
		current_student.update(student_params)
		redirect_to current_student
	end

	def student_params
		params.require(:student).permit(:name,:batch,:gender,:avatar,:college_id)
	end
end
