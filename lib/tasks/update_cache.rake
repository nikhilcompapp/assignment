task :update_cache =>[:environment] do |t|
    $redis.keys("current*_*").each do |elem|
        $redis.sadd(elem.split("_")[0],elem.split("_")[1])
    end
end