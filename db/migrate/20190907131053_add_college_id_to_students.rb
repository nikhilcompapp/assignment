class AddCollegeIdToStudents < ActiveRecord::Migration[5.2]
  def change
    add_column :students, :college_id, :string
  end
end
