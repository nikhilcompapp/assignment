class AddMoreFieldsToStudents < ActiveRecord::Migration[5.2]
  def change
    add_column :students, :name, :string
    add_column :students, :batch, :string
    add_column :students, :gender, :string
  end
end
