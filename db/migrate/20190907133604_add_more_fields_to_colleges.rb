class AddMoreFieldsToColleges < ActiveRecord::Migration[5.2]
  def change
    add_column :colleges, :name, :string
    add_column :colleges, :address, :string
    add_column :colleges, :current_views, :integer
    add_column :colleges, :total_views, :integer
  end
end
